#!/usr/bin/env bash

#
# Arch-Matic: Arch-Linux Postinstalación Configuración E Instalación De Software
# 

##	+-----------------------------------+-----------------------------------+
##	|                                                                       |
##	|                               ARCH-MATIC                              |
##	|                                                                       |
##	| Copyright (c) 2019, Juan Rodríguez <juferoga@vivaldi.net>.            |
##	|                                                                       |
##	| Esta Programa es software libre: tu puedes redistribuirlo y/o         |
##	| modificarlo esto bajo los terminos de la GNU General Public License   |
##	| publicada por la Free Software Foundation, ya sea la version 3 de     |
##	| la Licencia, o (a su elección) una más actual.                        |
##	|                                                                       |
##	| Este programa se distribuye con la esperanza de que sea útil,         |
##	| pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de      |
##	| COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver la     |
##	| GNU General Public License para más detalles.                         |
##	|                                                                       |
##	| Debería haber recibido una copia de la GNU General Public License     |
##	| junto con este programa. Si no, ver <http://www.gnu.org/licenses/>.   |
##	|                                                                       |
##	+-----------------------------------------------------------------------+

echo
echo "Instalando Componentes Para bluetooth"
echo

Paquetes=(

    'bluez'                 # Daemons for the bluetooth protocol stack
    'bluez-utils'           # Bluetooth development and debugging utilities
    'bluez-firmware'        # Firmwares for Broadcom BCM203x and STLC2300 Bluetooth chips
    'blueberry'             # Bluetooth configuration tool
    'pulseaudio-bluetooth'  # Bluetooth support for PulseAudio

        # Bibliotecas obsoletas para los protocolos bluetooth. 
        # Creo que el paquete de blues anterior es todo lo que se necesita ahora, 
        # pero no lo he probado, así que por ahora lo instalo también.
    #                        A
    #                        │
    'bluez-libs' # ──────────┘

)

for Paquete in "${Paquetes[@]}"; do
    echo
    echo " INSTALANDO : ${Paquete}"
    echo

    #Se realiza la instalación sin pedir confirmación y con las depencias necesarias
    sudo pacman -S "$Paquete" --noconfirm --needed
done

echo
echo "Los componentes de BLUETOOTH han sido instalados correctamente en su sistema!"
echo