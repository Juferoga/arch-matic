#!/usr/bin/env bash

#
# Arch-Matic: Arch-Linux Postinstalación Configuración E Instalación De Software
# 

##	+-----------------------------------+-----------------------------------+
##	|                                                                       |
##	|                               ARCH-MATIC                              |
##	|                                                                       |
##	| Copyright (c) 2019, Juan Rodríguez <juferoga@vivaldi.net>.            |
##	|                                                                       |
##	| Esta Programa es software libre: tu puedes redistribuirlo y/o         |
##	| modificarlo esto bajo los terminos de la GNU General Public License   |
##	| publicada por la Free Software Foundation, ya sea la version 3 de     |
##	| la Licencia, o (a su elección) una más actual.                        |
##	|                                                                       |
##	| Este programa se distribuye con la esperanza de que sea útil,         |
##	| pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de      |
##	| COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver la     |
##	| GNU General Public License para más detalles.                         |
##	|                                                                       |
##	| Debería haber recibido una copia de la GNU General Public License     |
##	| junto con este programa. Si no, ver <http://www.gnu.org/licenses/>.   |
##	|                                                                       |
##	+-----------------------------------------------------------------------+


##  +############################################    PASO 1     ############################################# - X +
##  |           REALIZAMOS la instlación con PACMAN sin tener que confirmar la instalación                        |
##  | Se instala el paquete PACMAN-CONTRIB (Scripts y herramientas hechas por la comunidad para sistemas pacman)  |
##  +-------------------------------------------------------------------------------------------------------------+

pacman -S --noconfirm pacman-contrib
echo
echo "PACMAN CONTRIB HA SIDO INSTALADO CON EXITO!!!"
echo

##   +###########################################    PASO 2      ############################################# - X+
##   | Creamos una copia de seguridad por si alguno de los mirrors no funcionan correctamente poder volver a los  |
##   |                                            originales                                                      |
##   +------------------------------------------------------------------------------------------------------------+

mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
echo
echo " LA COPIA DE SEGURIDAD DE mirrorlis HA SIDO GENERDA CON EXITO EN mirrorlist.backup !!!"
echo

##   +###########################################   PASO 3.1     ############################################# - X +
##   | Utilizamos CURL para transferir los archivos de los repos de Arch.org a nuestro PC los mirros de usa son un |
##   | Poco mas rápidos y seguros aunque los de Colombia tambien los pondre pero comentados(se pueden generar[VE]) |
##   +-------------------------------------------------------------------------------------------------------------+

##   +###########################################   PASO 3.2     ############################################# - X +
##   | Utilizamos SED para tommar los mirrors o los servidores que encuentre                                       |
##   +-------------------------------------------------------------------------------------------------------------+

##   +###########################################   PASO 3.2     ############################################# - X +
##   | Utilizamos RANKMIRRORS para que el gestor de paquetes(PACMAN) tome estos con mayor freccuencia que los pre- |
##   | determinados por Arch                                                                                       |
##   +-------------------------------------------------------------------------------------------------------------+

curl -s "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist
echo
echo "MIRRORS ACTUALIZADOS Y CONFIGURADOS CON EXITO !!!"
echo

read -r -p "Desea Configurar los mirrors para COLOMBIA? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    ############################ MIRRORS PARA COLOMBIA (solo hay uno q sad :'( #############################################################################################################################
    curl -s "https://www.archlinux.org/mirrorlist/?country=CO&protocol=http&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist
    echo
    echo "FIN DEL PROGRAMA :D, si desea continuar ejecute el segundo .SH"
    echo
else
    echo
    echo "FIN DEL PROGRAMA :D, si desea continuar ejecute el segundo .SH"
    echo
fi

# Mas info de la instalacion en la wiki :D