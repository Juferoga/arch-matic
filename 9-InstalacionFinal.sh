#!/usr/bin/env bash

#
# Arch-Matic: Arch-Linux Postinstalación Configuración E Instalación De Software
# 

##	+-----------------------------------+-----------------------------------+
##	|                                                                       |
##	|                               ARCH-MATIC                              |
##	|                                                                       |
##	| Copyright (c) 2019, Juan Rodríguez <juferoga@vivaldi.net>.            |
##	|                                                                       |
##	| Esta Programa es software libre: tu puedes redistribuirlo y/o         |
##	| modificarlo esto bajo los terminos de la GNU General Public License   |
##	| publicada por la Free Software Foundation, ya sea la version 3 de     |
##	| la Licencia, o (a su elección) una más actual.                        |
##	|                                                                       |
##	| Este programa se distribuye con la esperanza de que sea útil,         |
##	| pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de      |
##	| COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver la     |
##	| GNU General Public License para más detalles.                         |
##	|                                                                       |
##	| Debería haber recibido una copia de la GNU General Public License     |
##	| junto con este programa. Si no, ver <http://www.gnu.org/licenses/>.   |
##	|                                                                       |
##	+-----------------------------------------------------------------------+

echo
echo "Instalación y configurcion final"
echo

 ------------------------------------------------------------------------

echo
echo "Generando el archivo .xinitrc"
echo

# Al generar el archivo .xinitrc podremos ejecutar XFCE desde la
# terminal usando el comando "startx"

cat <<EOF > ${HOME}/.xinitrc
#!/bin/bash
if [ -d /etc/X11/xinit/xinitrc.d ] ; then
    for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
        [ -x "\$f" ] && . "\$f"
    done
    unset f
fi
source /etc/xdg/xfce4/xinitrc
exit 0
EOF

# ------------------------------------------------------------------------

echo
echo "Actualizando /bin/startx para usar la ruta apropiada"
echo

# Por default, startx busca incorrectamnte el archivo .serverauth en nuestra carpeta HOME.
sudo sed -i 's|xserverauthfile=\$HOME/.serverauth.\$\$|xserverauthfile=\$XAUTHORITY|g' /bin/startx

# ------------------------------------------------------------------------

echo
echo "Configurando el Kernel LTS como una segunda opcion de booteo"

sudo cp /boot/loader/entries/arch.conf /boot/loader/entries/arch-lts.conf
sudo sed -i 's|Arch Linux|Arch Linux LTS Kernel|g' /boot/loader/entries/arch-lts.conf
sudo sed -i 's|vmlinuz-linux|vmlinuz-linux-lts|g' /boot/loader/entries/arch-lts.conf
sudo sed -i 's|initramfs-linux.img|initramfs-linux-lts.img|g' /boot/loader/entries/arch-lts.conf

# ------------------------------------------------------------------------

echo
echo "Configurando MAKEPKG para usar todos los 4 nucleos"

sudo sed -i -e 's|[#]*MAKEFLAGS=.*|MAKEFLAGS="-j$(nproc)"|g' makepkg.conf
sudo sed -i -e 's|[#]*COMPRESSXZ=.*|COMPRESSXZ=(xz -c -T 4 -z -)|g' makepkg.conf

# ------------------------------------------------------------------------

echo
echo "Configurando vconsole.conf para poner otra fuente para login shell"

sudo cat <<EOF > /etc/vconsole.conf
KEYMAP=la-latin1
FONT=ter-v32b
EOF

# ------------------------------------------------------------------------

echo
echo "Deshabilita el dibujado del cursor con errores"

# Cuando tu inicias con varias pantallas el cursor puede lucir enorme. Esto arregla eso.
sudo cat <<EOF > /usr/share/icons/default/index.theme
[Icon Theme]
#Inherits=Theme
EOF

# ------------------------------------------------------------------------

echo
echo "Aumentando el contador de usarios que miran archivos"

# Esto previene el error "too many files" en Visual Studio Code
echo fs.inotify.max_user_watches=524288 | sudo tee /etc/sysctl.d/40-max-user-watches.conf && sudo sysctl --system

# ------------------------------------------------------------------------

echo
echo "Deshabilitando Pulse .esd_auth modulo"

# Pulse audio carga el modulo `esound-protocol`, lo mejor que puedo decir es que rara vez se necesita.
# Este modulo crea un archivo llamado `.esd_auth` en el directorio home que preferiría no estar allí. 
# Entonces procedemos a...
sudo sed -i 's|load-module module-esound-protocol-unix|#load-module module-esound-protocol-unix|g' /etc/pulse/default.pa

# ------------------------------------------------------------------------

echo
echo "Habilitando el Demonio de bluetooth y configurandolo para un auto-arranque"

sudo sed -i 's|#AutoEnable=false|AutoEnable=true|g' /etc/bluetooth/main.conf
sudo systemctl enable bluetooth.service
sudo systemctl start bluetooth.service

# ------------------------------------------------------------------------

echo
echo "Habilitando el Demonio de cups para poder imprimir"

systemctl enable org.cups.cupsd.service
systemctl start org.cups.cupsd.service

# ------------------------------------------------------------------------

echo
echo "Habilitando el Protocolo de hora de red para que la hora se sincronice con la red"

sudo ntpd -qg
sudo systemctl enable ntpd.service
sudo systemctl start ntpd.service

# ------------------------------------------------------------------------

echo
echo "CONFIGURACION DE RED"
echo
echo "Busca el nombre de su enlace IP:"
echo

ip link

echo
read -p "Ingresa tu IP LINK: " LINK

echo
echo "Deshabilita DHCP y habilita el demonio de Network Manager"
echo

sudo systemctl disable dhcpcd.service
sudo systemctl stop dhcpcd.service
sudo ip link set dev ${LINK} down
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service
sudo ip link set dev ${LINK} up

echo "Se ha terminado la instalación correctamente!"
echo 
echo "Puedes reiniciar ahora..."
echo

read -r -p "Desea REINICIAR Ahora? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    neofetch
    echo
    echo "FIN DEL PROGRAMA :D"
    echo
    reboot
else
    neofetch
fi
