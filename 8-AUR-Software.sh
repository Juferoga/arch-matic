#!/usr/bin/env bash

#
# Arch-Matic: Arch-Linux Postinstalación Configuración E Instalación De Software
# 

##	+-----------------------------------+-----------------------------------+
##	|                                                                       |
##	|                               ARCH-MATIC                              |
##	|                                                                       |
##	| Copyright (c) 2019, Juan Rodríguez <juferoga@vivaldi.net>.            |
##	|                                                                       |
##	| Esta Programa es software libre: tu puedes redistribuirlo y/o         |
##	| modificarlo esto bajo los terminos de la GNU General Public License   |
##	| publicada por la Free Software Foundation, ya sea la version 3 de     |
##	| la Licencia, o (a su elección) una más actual.                        |
##	|                                                                       |
##	| Este programa se distribuye con la esperanza de que sea útil,         |
##	| pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de      |
##	| COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver la     |
##	| GNU General Public License para más detalles.                         |
##	|                                                                       |
##	| Debería haber recibido una copia de la GNU General Public License     |
##	| junto con este programa. Si no, ver <http://www.gnu.org/licenses/>.   |
##	|                                                                       |
##	+-----------------------------------------------------------------------+

echo
echo "Instalando Aplicaciones de AUR"
echo

cd "${HOME}"

echo "CLONANDO YAY: YAY"
git clone "https://aur.archlinux.org/yay.git"
echo "CLONADO PPRO"

Paquetes=(
    # UTILIDADES PARA EL SISTEMA -----------------------------------------

#    'menulibre'                 # Menu editor
#    'gtkhash'                   # Checksum verifier

    # UTILIDADES PARA LA TERMINAL -----------------------------------------

#    'hyper'                     # Terminal emulator built on Electron
#    'cmatrix'                   # Tu terminal hacker

    # ADS-ON --------------------------------------------------------------

#    'flameshot'                 # Capturs de pantalla FULLHD
#    'dropbox'                   # Cloud file storage
#    'lastpass'                  # Password manager
#    'slimlock'                  # Screen locker
#    'oomox'                     # Theme editor
#    'samba'                     # Samba File Sharing
#    'gnome-vfs'                 # Gnome File System Access
#    'gvfs-smb'                  # More File System Stuff
#    'xclip'                     # Nuestro Portapapeles
#    'synapse'                   # Buscador de archivos <3
                #Para synapse <3
#    'zeal'                      # Busca en la documentacion de un lenguaje de programacion

    # DESARROLLO ----------------------------------------------------------
    
    'visual-studio-code-bin'    # Kickass text editor

    # GAME ----------------------------------------------------------------

#    'lutris'                    # Para ejecutr juegos tanto de win y linux

    # MEDIA ---------------------------------------------------------------

#    'screenkey'                 # Screencast your keypresses
#    'aftershotpro3'             # Photo editor
#    'vvave'                     # Reproductor de sonido <3
#    'inkscape'                  # Vector Desing

    # POST PRODUCTION -----------------------------------------------------

#    'peek'                      # GIF animation screen recorder

    # INTERNET ------------------------------------------------------------

#    'google-chrome'             # Google Chrome
#    'vivaldi'                   # Vivaldi web browser  <3
#    'firefox'                   # Firefox Browser   <3
    

    # TEMAS ---------------------------------------------------------------

#    'gtk-theme-arc-git'
#    'adapta-gtk-theme-git'
#    'paper-icon-theme'
#    'tango-icon-theme'
#    'tango-icon-theme-extras'
#    'numix-icon-theme-git'
#    'papirus-icon-theme' 
#    'sardi-icons'
#    'alacarte-xfce'
#    'mugshot'
)

cd ${HOME}/yay
makepkg -si

for Paquete in "${Paquetes[@]}"; do
    yay -S --noconfirm $Paquete
done

echo
echo " ha sido instalado correctamente en su sistema!"
echo
