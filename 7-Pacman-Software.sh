#!/usr/bin/env bash

#
# Arch-Matic: Arch-Linux Postinstalación Configuración E Instalación De Software
# 

##	+-----------------------------------+-----------------------------------+
##	|                                                                       |
##	|                               ARCH-MATIC                              |
##	|                                                                       |
##	| Copyright (c) 2019, Juan Rodríguez <juferoga@vivaldi.net>.            |
##	|                                                                       |
##	| Esta Programa es software libre: tu puedes redistribuirlo y/o         |
##	| modificarlo esto bajo los terminos de la GNU General Public License   |
##	| publicada por la Free Software Foundation, ya sea la version 3 de     |
##	| la Licencia, o (a su elección) una más actual.                        |
##	|                                                                       |
##	| Este programa se distribuye con la esperanza de que sea útil,         |
##	| pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de      |
##	| COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver la     |
##	| GNU General Public License para más detalles.                         |
##	|                                                                       |
##	| Debería haber recibido una copia de la GNU General Public License     |
##	| junto con este programa. Si no, ver <http://www.gnu.org/licenses/>.   |
##	|                                                                       |
##	+-----------------------------------------------------------------------+

echo
echo "Instalando Aplicaciones"
echo

Paquetes=(
    # SISTEMA --------------------------------------------------------------

    'linux-lts'             # Long term support kernel

    # COMPLEMENTOS DE LA TERMINAL -------------------------------------------

    'bash-completion'       # Tab completion for Bash
    'bleachbit'             # File deletion utility
    'cronie'                # cron jobs
    'curl'                  # Remote content retrieval
    'file-roller'           # Archive utility
    'gtop'                  # System monitoring via terminal
    'gufw'                  # Firewall manager
    'hardinfo'              # Hardware info app
    'htop'                  # Process viewer
    'neofetch'              # Shows system info when you launch terminal
    'ntp'                   # Network Time Protocol to set time via network.
    'numlockx'              # Turns on numlock in X11
    'openssh'               # SSH connectivity tools
    'p7zip'                 # 7zip para varios tipos de archivos comprimidos
    'rsync'                 # Remote file sync utility
    'speedtest-cli'         # Internet speed via terminal
    'terminus-font'         # Font package with some bigger fonts for login terminal
    'tlp'                   # Advanced laptop power management
    'unrar'                 # RAR compression program
    'unzip'                 # Zip compression program
    'wget'                  # Remote content retrieval
    'terminator'            # Terminal emulator
    'zenity'                # Display graphical dialog boxes via shell scripts
    'zip'                   # Zip compression program
    'zsh'                   # ZSH shell
    'zsh-completions'       # Tab completion for ZSH

    # UTILIDADES DISCOS ---------------------------------------------------

    'autofs'                # Auto-mounter
    'exfat-utils'           # Mount exFat drives
    'gparted'               # Disk utility
    'ntfs-3g'               # Open source implementation of NTFS file system
    'parted'                # Disk utility

    # UTILIDADES EN GENERAL ------------------------------------------------

    'catfish'               # Busqueda de archivos
    'conky'                 # Informacion del Sistema
    'nautilus'              # Archivos
    'veracrypt'             # Disc encrypt
    'variety'               # Wallpapers
    'xfburn'                # CD burning application
    'docky'                 # Dock Options

    # DESARROLLO ------------------------------------------------------------

    'kate'                  # Editor de texto
    'clang'                 # C Lang compiler
    'cmake'                 # Cross-platform open-source make system
    'electron'              # Cross-platform development using Javascript
    'git'                   # Version control system
    'gcc'                   # C/C++ compiler
    'glibc'                 # C libraries
    'meld'                  # File/directory comparison
    'nodejs'                # Javascript runtime environment
    'npm'                   # Node package manager
    'python'                # Scripting language
    'qtcreator'             # C++ cross platform IDE
    'qt5-examples'          # Project demos for Qt
    'yarn'                  # Dependency management (Hyper needs this)

    # HERRAMIENTAS WEB ------------------------------------------------------

    'filezilla'             # Cliente FTP 
    
    # COMUNICACIONES --------------------------------------------------------

    'hexchat'               # Chat
    
    # MEDIA -----------------------------------------------------------------

    'kdenlive'              # Editor de video
    'obs-studio'            # Graba tu screen
    'vlc'                   # Video player
    
    # GRAFICOS Y DISEÑO ------------------------------------------------------

    'gcolor2'               # Colorpicker
    'gimp'                  # GNU Image Manipulation Program
    'ristretto'             # Multi image viewer

    # PRODUCTIVIDAD -----------------------------------------------------------

    'hunspell'              # Spellcheck libraries
    'hunspell-en'           # English spellcheck library
    'libreoffice-fresh'     # Libre office con cosas extra
    'xpdf'                  # PDF 

    # VIRTUALIZACION ------------------------------------------------------

    'virtualbox'
    'virtualbox-host-modules-arch'
)

for Paquete in "${Paquetes[@]}"; do
    echo
    echo " INSTALANDO : ${Paquete}"
    echo

    #Se realiza la instalación sin pedir confirmación y con las depencias necesarias
    sudo pacman -S "$Paquete" --noconfirm --needed
done

echo
echo " ha sido instalado correctamente en su sistema!"
echo