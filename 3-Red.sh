#!/usr/bin/env bash

#
# Arch-Matic: Arch-Linux Postinstalación Configuración E Instalación De Software
# 

##	+-----------------------------------+-----------------------------------+
##	|                                                                       |
##	|                               ARCH-MATIC                              |
##	|                                                                       |
##	| Copyright (c) 2019, Juan Rodríguez <juferoga@vivaldi.net>.            |
##	|                                                                       |
##	| Esta Programa es software libre: tu puedes redistribuirlo y/o         |
##	| modificarlo esto bajo los terminos de la GNU General Public License   |
##	| publicada por la Free Software Foundation, ya sea la version 3 de     |
##	| la Licencia, o (a su elección) una más actual.                        |
##	|                                                                       |
##	| Este programa se distribuye con la esperanza de que sea útil,         |
##	| pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de      |
##	| COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver la     |
##	| GNU General Public License para más detalles.                         |
##	|                                                                       |
##	| Debería haber recibido una copia de la GNU General Public License     |
##	| junto con este programa. Si no, ver <http://www.gnu.org/licenses/>.   |
##	|                                                                       |
##	+-----------------------------------------------------------------------+

echo
echo "Instalando Los Componentes De Red"
echo

Paquetes=(
        'wpa_supplicant'            # Negociador de claves a conexiones inalambricas de tipo WPA
        'dialog'                    # Habilita a los shellscripts a ejecutar cajas de dialogo
        'networkmanager'            # Administrador de conexiones 
        'openvpn'                   # Soporte de Open VPN
        'networkmanager-openvpn'    # Open VPN complemento para NM
        'networkmanager-vpnc'       # Open VPN complemento para NM. Probablemente no lo necesites si networkmanager-openvpn esta instalado.
        'network-manager-applet'    # Administrador de conexiones gráfico
        'dhclient'                  # Cliente DHCP
        'libsecret'                 # Libreria para almacenamiento de contraseñas
)

for Paquete in "${Paquetes[@]}"; do
    echo
    echo " INSTALANDO : ${Paquete}"
    echo

    #Se realiza la instalación sin pedir confirmación y con las depencias necesarias
    sudo pacman -S "$Paquete" --noconfirm --needed
done

echo
echo "Los componentes de RED han sido instalados correctamente en su sistema!"
echo